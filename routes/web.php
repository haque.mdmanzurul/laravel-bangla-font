<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	//PDF::setOptions(['dpi' => 150, 'defaultFont' => 'Hind Siliguri']);
	$pdf = PDF::loadView('pdf');
	return $pdf->inline();
	//return $pdf->download('bangla.pdf');
    //return view('pdf');
});
